#!/usr/bin/env python
import glob
import subprocess
import click
import astropy.io.fits as fits
import os
import numpy as np
import astropy.table as table
import matplotlib.pyplot as plt


@click.command()
@click.option('--wlen-min', type=float, default=4000)
@click.option('--ivar', is_flag=True)
@click.option('--noise-column', default='OPT_FLAM_SIG')
@click.option('--flux-column', default='OPT_FLAM')
@click.option('--wave-column', default='OPT_WAVE')
@click.argument('fname')
def main(fname, wlen_min, flux_column, wave_column, noise_column, ivar):
    h = fits.open(fname)
    d = h[1].data
    
    wave = d[wave_column]
    flux = d[flux_column]
    if ivar:
        noise = 1/d[noise_column]
    else:
        noise = d[noise_column]
    
    idx = wave > wlen_min
    
    plt.plot(wave[idx], flux[idx]/noise[idx])
    plt.show()



if __name__ == '__main__':
    main()
