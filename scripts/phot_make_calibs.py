#!/usr/bin/env python3
import click, subprocess, glob, sys, os
import numpy as np
import astropy.io.fits as fits
import astropy.time as time


REQ_KEYS = [
    'DATE-OBS',
    'OBS_MODE',
    'IMAGETYP',
    'DETWIN1',
    'DETXBIN',
    'DETYBIN'
]


def get_filter_name(hdr):
    if 'ALFLTNM' in hdr and hdr['ALFLTNM'] != 'Open':
        return hdr['ALFLTNM']
    if 'FAFLTNM' in hdr and hdr['FAFLTNM'] != 'Open':
        return hdr['FAFLTNM']
    if 'FBFLTNM' in hdr and hdr['FBFLTNM'] != 'Open':
        return hdr['FBFLTNM']
    return ''


@click.command()
@click.option('--detwin', default="[1:2148, 1:2102]")
@click.option('--detbin', default=1)
@click.argument("day")
def main(day, detwin, detbin):
    t = time.Time(day)
    
    dest_dir = 'phot/calibs/%s' % day

    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    bias_frames = []

    flat_frames = {}
    
    files = glob.glob('raw/%s/*.fits' % day)
    # load headers
    for fname in files:
        hdr = fits.getheader(fname)

        # make sure we have all the key words we _really_ need
        have_all_keys = True
        for k in REQ_KEYS:
            if k not in hdr:
                have_all_keys = False
        if not have_all_keys:
            continue

        frame_t = time.Time(hdr['DATE-OBS'])
        if np.abs(t.mjd-frame_t.mjd) > 3:
            # actually doesnt belong here..
            continue

        if hdr['DETWIN1'] != detwin:
            continue

        if hdr['DETXBIN'] != detbin:
            continue
        if hdr['DETYBIN'] != detbin:
            continue
        
        if hdr['IMAGETYP'] == 'BIAS':
            bias_frames.append(fname)
            continue

        if hdr['OBS_MODE'] != 'IMAGING':
            continue

        filter_name = get_filter_name(hdr).replace("'", '').replace(' ', '_')
        if hdr['IMAGETYP'] == 'FLAT,SKY':
            if filter_name not in flat_frames:
                flat_frames[filter_name] = []
            flat_frames[filter_name].append(fname)

    # generate the master bias
    master_bias_fname = '%s/master_bias.fits' % dest_dir
    if len(bias_frames) < 3:
        print('Not enough bias frames!')
        sys.exit(1)
    subprocess.check_call("ficombine -o %s -m rejmed %s" % (master_bias_fname, ' '.join(bias_frames)), shell=True)

    for filter_name in flat_frames.keys():
        if filter_name == '':
            continue
        flat_fname = '%s/master_flat_%s.fits' % (dest_dir, filter_name)
        subprocess.check_call('python3 scripts/phot_make_master_flat.py -o %s -b %s %s' % (flat_fname, master_bias_fname, ' '.join(flat_frames[filter_name])), shell=True)
    
    


if __name__ == '__main__':
    main()
