#!/usr/bin/env python
import click
import astropy.io.fits as fits
import os
import numpy as np
import astropy.table as table
import matplotlib.pyplot as plt

@click.command()
@click.option('--wlen-min', type=float, default=4000)
@click.argument('filename')
def main(filename, wlen_min):
    h = fits.open(filename)

    #print(h.info())

    wave = h['WAVE'].data
    sensfunc = h['SENSFUNC'].data

    idx = wave > wlen_min
    
    plt.plot(wave[idx], sensfunc[idx])
    plt.show()


if __name__ == '__main__':
    main()
