#!/usr/bin/env python
import subprocess
import click
import os
import astropy.table as table


@click.command()
@click.option('-o', '--output', required = True)
@click.option(      '--objid')
@click.argument('spectra', nargs=-1)

def main(spectra, output, objid):
    if len(spectra) < 2:
        raise ValueError('Need at least two input spectra!')

    if os.path.isfile(output):
        raise ValueError('Destination file already exists!')
    
    # find the objid for each input spectrum
    objids = []
    for fname in spectra:
        fname_txt = fname.replace('.fits', '.txt')
        cat = table.Table.read(fname_txt, format='ascii.fixed_width')

        if len(cat) > 1 and objid is None:
            print(cat)
            raise ValueError('Pypeit extracted multiple spectra.. Please specify the ID with \'--objid\'')

        if len(cat) == 1:
            objid = 0
        try:
            objids.append(cat['name'][int(objid)])
        except ValueError:
            if objid not in cat['name']:
                raise ValueError('objid %s not found in catalog %s' % (objid, fname_txt))
            objids.append(objid)
        

    with open('combine.par', 'w') as f:
        f.write('[coadd1d]\n')
        f.write('coaddfile=%s\n' % output)
        f.write('\n')
        f.write('coadd1d read\n')
        for spec,objid in zip(spectra, objids):
            f.write('  %s %s\n' % (spec, objid))
        f.write('coadd1d end\n')
    
    subprocess.run(['pypeit_coadd_1dspec', 'combine.par'])

    


if __name__ == '__main__':
    main()
