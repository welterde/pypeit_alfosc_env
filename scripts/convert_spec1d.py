#!/usr/bin/env python
import glob
import subprocess
import click
from   astropy.io import fits
import os
import numpy as np
import astropy.table as table


@click.command()
@click.option('--objid')
@click.option('--wlen-min', type=float, default=4000)
@click.option('--column', default='OPT_FLAM')
@click.option('--wave-column', default='OPT_WAVE')
@click.option('--err-column', default='OPT_FLAM_IVAR')
@click.option('--output-format', default='ascii')
@click.argument('frames', nargs=-1)

def main(frames, wlen_min, column, wave_column, err_column, output_format, objid):
    if output_format != 'ascii':
        raise ValueError('Unsuported output format!')
    
    for fname in frames:
        new_fname = os.path.basename(fname).replace('.fits', '.ascii')

        h = fits.open(fname)
        if objid:
            d = h[objid].data
        else:
            d = h[1].data

        wave = d[wave_column]
        flux = d[column]
        err  = d[err_column]

        idx = wave > wlen_min
        
        t = table.Table([wave[idx], flux[idx], err[idx]], names=('wave', 'flux', 'err'))
        t['err'] = pow(t['err'], -0.5)
        t.meta['comments'] = list([key + ' = ' +  str(h[0].header[key]) for key in list(h[0].header.keys())])
        t.write(new_fname, format='ascii.no_header')

if __name__ == '__main__':
    main()
