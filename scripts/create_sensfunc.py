#!/usr/bin/env python
import glob
import subprocess
import click
import astropy.io.fits as fits
import os


@click.command()
@click.argument('day')
def main(day):
    print(' * Searching for std frames on %s' % day)
    std_1dframes = glob.glob('sci/%s-STD-*/spec1d*.fits' % day)

    
    for frame in std_1dframes:
        hdr = fits.getheader(frame)
        mjd = hdr['MJD']
        dest_path = 'sens/%.4f.fits' % mjd
        print(' * %s -> %s' % (frame, dest_path))
        if os.path.isfile(dest_path):
            print('   * ERR: already exists!')
            continue
        subprocess.run(['pypeit_sensfunc', '-s', 'etc/sensfunc.par', frame, '-o', dest_path])




if __name__ == '__main__':
    main()
