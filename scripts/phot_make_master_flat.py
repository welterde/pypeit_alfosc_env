#!/usr/bin/env python3
import sys,ccdproc, click
import astropy.nddata as nddata
import astropy.io.fits as fits
import numpy as np
from astropy.stats import mad_std


def inv_median(a):
    return 1 / np.median(a)

@click.command()
@click.option('-o', '--outfile')
@click.option('-b', '--bias-frame')
@click.argument('raw_frames', nargs=-1)
def main(raw_frames, outfile, bias_frame):
    # load master bias frame
    master_bias = nddata.fits_ccddata_reader(bias_frame, unit='adu')
    
    # load frames
    ccd_frames = []
    for fname in raw_frames:
        hdr = fits.getheader(fname)
        #print(fname)
        frame = nddata.fits_ccddata_reader(fname, hdu='im1', unit='adu')
        
        if hdr['DETXBIN'] != 1:
            continue
        if hdr['DETYBIN'] != 1:
            continue
        # do basic CCD processing
        new_frame = ccdproc.ccd_process(frame, master_bias=master_bias)
        new_frame.mask = new_frame.data < 4000
        #newnew_frame = nddata.CCDData(data=new_frame.data, mask=mask, wcs=new_frame.wcs, meta=new_frame.meta, unit=new_frame.unit)
        #print(new_frame.mask)
        ccd_frames.append(new_frame)

    # combine to master flat
    master_flat = ccdproc.combine(ccd_frames, method='median', scale=inv_median,
                                  sigma_clip=True, sigma_clip_low_thresh=3, sigma_clip_high_thresh=3,
                                  sigma_clip_func=np.ma.median, signma_clip_dev_func=mad_std,
                                  mem_limit=350e6)
    #combiner = ccdproc.Combiner(ccd_frames)
    #combiner.sigma_clipping(3, 5, func=np.ma.median, dev_func=mad_std)
    #master_flat = combiner.average_combine(scale_func=inv_median)
    #print(master_flat.mask)
    master_flat.write(outfile, overwrite=True)
                                

    
    



if __name__ == '__main__':
    main()
