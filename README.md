# Installation

First you need to install pipenv and python3

> pipenv install

# Usage

All commands need to be executed with the current working directory being in the top directory of this git repository (directory where this README is located).

## Creating Datasets

1. Download data (science + calib) to raw/YYYY-MM-DD/
2. Run:
> pipenv run scripts/create_datasets.py 2020-07-07

This will create the relevant datasets for science and standards in datasets

## Reducing all datasets

> pipenv run run_pypeit datasets/2020-07-07-STD-SP2209+178.pypeit

(same for each dataset)

## Creating sensitivity function

This will automatically match all standard star targets that were reduced (or try to anyway)

> pipenv run scripts/create_sensfunc.py 2020-07-07

This will create a new sensitivity file for the MJD and place it into sens

## Flux calibrating spectra

This will automatically search for the correct sens file in the sens dir and apply it to the spectra

> pipenv run scripts/apply_rough_fluxcal.py sci/2020-07-07-*/spec1d*.fits

## Looking at specs

> pipenv run pypeit_show_1dspec sci/*/spec1d_ALDg070110-ZTF20abfehpe_ALFOSC_2020Jul08T000441.463.fits --flux

## Combining specs

> pipenv run scripts/combine_spectra.py -o ztf20aau_combine.fits sci/2020-07-07-ZTF20aauoktk/spec1d_ALDg07009*.fits

Will combine the spec1d frames into the destination file specified with -o

In case multiple spectra were extracted one has to select the correct one with --objid <number> (it will print a table of options)

## Converting to ASCII

> pipenv run scripts/convert_spec1d.py sci/2020-07-07-ZTF20aauoktk/spec1d_ALDg07009*.fits

Will create spec1d_ALDg07009*.ascii in the local directory.
Spectral range can be tweaked with --wlen-min

In case of converting a combined spectrum one needs to explicitly state the flux and wavelength columns:

> pipenv run scripts/convert_spec1d.py ztf20aau_combine.fits --column flux --wave-column wave --err-column ivar

## Plotting

> pipenv run scripts/plot_sens.py sens/59038.2281.fits

To check if the sensitivity function makes sense (ha!)

> pipenv run scripts/plot_snr.py sci/*/spec1d_ALDg070088-ZTF18acqugen_ALFOSC_2020Jul07T221416.630.fits

To plot SNR over wavelength for a spec



# Photometry

Calibs and science frames must be in raw/YYYY-MM-DD

## Install

Requires sextractor, scamp and swarp to be installed (see astromatic.net).
Requires fitsh and qfits-tools to be installed.

On debian:

> sudo apt install fitsh qfits-tools
> sudo apt install sextractor scamp swarp

## Createing calibs

> pipenv run python3 scripts/phot_make_calibs.py 2020-08-23

Output will be in phot/calibs/YYY-MM-DD

## Reduce object

> sh scripts/phot_sci_object.sh 2020-09-20 ZTF19abtsnyy

Detrends, does astrometry and stacks the frames.

Stacked frame: phot/stacked/<OBJECT>/<DATE>_<FILTER>.fits
